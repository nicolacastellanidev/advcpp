# Advanced C++ Final Exam

***Nicola Castellani – VR466107***

**Final** **exam** of *Master Game Dev Advanced c++ Course*.

***Author***: nicolacastellanidev@gmail.com
  
---

## MemoryManagement

An implementation of custom memory manager using the classic ***SmallObjectAllocator*** by Andrei Alexandrescu and a ***MemoryManager*** with a ***free list allocator***.

### Getting Started

Run ***main.cpp*** under **AdvCpp_EOT** solution. The main() method will run some tests to check if allocator fails.

*You can find more informations about the project under the dedicated README* 

---

## BigInt

A custom ***BigInteger*** implementation using **digit** split logic.

### Getting Started

Run ***main.cpp*** under **BigInt** solution. The main() method will run some tests to check if allocator fails.

*You can find more informations about the project under the dedicated README* 