#include "pch.h"
#include "SmallObjectAllocator.h"
#include "FixedAllocator.h"
#include "../Nica/Print.h"
#include "Shared.h"

#include <functional>
#include <algorithm>
#include <cassert>
#include <iostream>
#include <string>

Nica::SmallObjectAllocator::SmallObjectAllocator()
	:_LastAllocation(0), _LastDeallocation(0)
	, _ChunkSize(DEFAULT_CHUNK_SIZE), _MaxObjectSize(MAX_SMALL_OBJECT_SIZE) {}

Nica::SmallObjectAllocator::SmallObjectAllocator(std::size_t ChunkSize, std::size_t MaxObjectSize)
	: _LastAllocation(0), _LastDeallocation(0)
	, _ChunkSize(ChunkSize), _MaxObjectSize(MaxObjectSize) {}

void* Nica::SmallObjectAllocator::Allocate(std::size_t NumBytes)
{
	if (NumBytes > _MaxObjectSize) {
		return malloc(NumBytes);
	}

	if (_LastAllocation && _LastAllocation->BlockSize() == NumBytes)
	{
		return _LastAllocation->Allocate();
	}

	// Best fit
	Pool::iterator i = std::lower_bound(_Pool.begin(), _Pool.end(), NumBytes, CompareFixedAllocatorSize());

	if (i == _Pool.end() || i->BlockSize() != NumBytes)
	{
		i = _Pool.insert(i, FixedAllocator(NumBytes, _ChunkSize));
		_LastDeallocation = &*_Pool.begin();
	}
	_LastAllocation = &*i;

	void* result = _LastAllocation->Allocate();

#ifdef DEBUG_LOG
	Nica::PrintSpaceBetween(
		{ "[SmallObjAllocator]", "[New allocation]" },
		{ "[Pointer]", std::to_string(reinterpret_cast<uintptr_t>(result)), " [Size] ", std::to_string(NumBytes)});
#endif

	return result;
}

void Nica::SmallObjectAllocator::Deallocate(void* p, std::size_t NumBytes)
{
	if (NumBytes > _MaxObjectSize) return free(p);

	if (_LastDeallocation && _LastDeallocation->BlockSize() == NumBytes)
	{
		_LastDeallocation->Deallocate(p);
		return;
	}
	Pool::iterator i = std::lower_bound(_Pool.begin(), _Pool.end(), NumBytes,
		CompareFixedAllocatorSize());
	assert(i != _Pool.end());
	assert(i->BlockSize() == NumBytes);
	_LastDeallocation = &*i;
	_LastDeallocation->Deallocate(p);
}

bool Nica::CompareFixedAllocatorSize::operator()(const FixedAllocator& x, std::size_t numBytes) const
{
	return x.BlockSize() < numBytes;
}