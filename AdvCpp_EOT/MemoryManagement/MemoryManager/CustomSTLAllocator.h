#pragma once
#include "MemoryManager.h"
#include "Shared.h"

#include <iostream>
#include <string>

namespace Nica {
	template <typename T>
	class CustomSTLAllocator
	{
	public:
		typedef size_t size_type;
		typedef ptrdiff_t difference_type;
		typedef T* pointer;
		typedef const T* const_pointer;
		typedef T& reference;
		typedef const T& const_reference;
		typedef T value_type;

		CustomSTLAllocator() {}
		~CustomSTLAllocator() {}

		template <class U> struct rebind { typedef CustomSTLAllocator<U> other; };
		template <class U> CustomSTLAllocator(const CustomSTLAllocator<U>&) {}

		pointer address(reference x) const { return &x; }
		const_pointer address(const_reference x) const { return &x; }

		size_type max_size() const throw() { return size_t(-1) / sizeof(value_type); }

		pointer allocate(size_type allocation_size, const void* hint = 0)
		{
			return reinterpret_cast<pointer>(Nica::MemoryManager::Get()->MM_NEW(allocation_size * sizeof(T)));
		}

		void deallocate(pointer p, size_type deallocation_size)
		{
			Nica::MemoryManager::Get()->MM_DELETE(p, sizeof(T));
		}

		void construct(pointer p, const T& val)
		{
			new(static_cast<void*>(p)) T(val);
		}

		void construct(pointer p)
		{
			new(static_cast<void*>(p)) T();
		}

		void destroy(pointer p)
		{
			p->~T();
		}
	};
}