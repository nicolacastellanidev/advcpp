#include "pch.h"
#include "FreeListAllocator.h" 
#include "../Nica/Print.h"
#include "PointerMath.h"
#include "Shared.h"

#include<string>
#include <cassert>

Nica::FreeListAllocator::FreeListAllocator(size_t Size, void* Start)
	: BaseAllocator(Size), _FreeBlocks((FreeBlock*)Start)
{
	assert(Size > sizeof(FreeBlock));
	_FreeBlocks->Size = Size;
	_FreeBlocks->Next = nullptr;
}

Nica::FreeListAllocator::~FreeListAllocator()
{
	_FreeBlocks = nullptr;
}

void* Nica::FreeListAllocator::Allocate(size_t Size, u8 Alignment)
{
	assert(Size != 0 && Alignment != 0);

	FreeBlock* PreviousBlock = nullptr;
	FreeBlock* CurrentBlock = _FreeBlocks; // get pointer to first free block

	FreeBlock* BestFistPrevious = nullptr;
	FreeBlock* BestFit = nullptr;
	u8         BestFitAdjustment = 0;
	size_t     BestFitTotalSize = 0;

	//Find best fit
	while (CurrentBlock != nullptr)
	{
		//Calculate adjustment needed to keep object correctly aligned
		u8 Adjustment = PointerMath::alignForwardAdjustmentWithHeader<AllocationHeader>(CurrentBlock, Alignment); // align with header

		size_t TotalSize = Size + Adjustment;

		//If its an exact match use this free block
		if (CurrentBlock->Size == TotalSize)
		{
			BestFistPrevious = PreviousBlock;
			BestFit = CurrentBlock;
			BestFitAdjustment = Adjustment;
			BestFitTotalSize = TotalSize;
			break;
		}

		//If its a better fit switch
		if (CurrentBlock->Size > TotalSize && (BestFit == nullptr || CurrentBlock->Size < BestFit->Size))
		{
			BestFistPrevious = PreviousBlock;
			BestFit = CurrentBlock;
			BestFitAdjustment = Adjustment;
			BestFitTotalSize = TotalSize;
		}

		PreviousBlock = CurrentBlock;
		CurrentBlock = CurrentBlock->Next;
	}

	if (BestFit == nullptr)
		return nullptr;

	//If allocations in the remaining memory will be impossible
	if (BestFit->Size - BestFitTotalSize <= sizeof(AllocationHeader))
	{
		//Increase allocation size instead of creating a new FreeBlock
		BestFitTotalSize = BestFit->Size;

		if (BestFistPrevious != nullptr)
			BestFistPrevious->Next = BestFit->Next;
		else
			_FreeBlocks = BestFit->Next;
	}
	else
	{
		//Prevent new block from overwriting best fit block info
		assert(BestFitTotalSize > sizeof(FreeBlock));

		//Else create a new FreeBlock containing remaining memory
		FreeBlock* NewBlock = (FreeBlock*)(PointerMath::add(BestFit, BestFitTotalSize));
		NewBlock->Size = BestFit->Size - BestFitTotalSize;
		NewBlock->Next = BestFit->Next;

		if (BestFistPrevious != nullptr)
			BestFistPrevious->Next = NewBlock;
		else
			_FreeBlocks = NewBlock;
	}

	uptr AlignedAddress = (uptr)BestFit + BestFitAdjustment;

	AllocationHeader* header = (AllocationHeader*)(AlignedAddress - sizeof(AllocationHeader));
	header->Size = BestFitTotalSize;
	header->Adjustment = BestFitAdjustment;

	assert(PointerMath::isAligned(header));

	_UsedMemory += BestFitTotalSize;
	_NumAllocations++;

	assert(PointerMath::alignForwardAdjustment((void*)AlignedAddress, Alignment) == 0);

#ifdef DEBUG_LOG
	Nica::PrintSpaceBetween(
		{ "[FreeListAllocator]", "[ALLOCATE]" },
		{ "Address: ", std::to_string(AlignedAddress), " Alignment: ", std::to_string(Alignment) }
	);
#endif

	return (void*)AlignedAddress;
}

void Nica::FreeListAllocator::Deallocate(void* p)
{
	assert(p != nullptr);

	AllocationHeader* Header = (AllocationHeader*)PointerMath::subtract(p, sizeof(AllocationHeader));

	uptr   BlockStart = reinterpret_cast<uptr>(p) - Header->Adjustment;
	size_t BlockSize = Header->Size;
	uptr   BlockEnd = BlockStart + BlockSize;

	assert(BlockEnd - BlockStart == Header->Size);

#ifdef DEBUG_LOG
	Nica::PrintSpaceBetween(
		{ "[FreeListAllocator]", "[DEALLOCATE]" },
		{ "Address: ", std::to_string(BlockStart), " Of size: ", std::to_string(BlockSize) }
	);
#endif
	FreeBlock* PreviousFreeBlock = nullptr;
	FreeBlock* CurrentBlock = _FreeBlocks;

	// loop through each block
	while (CurrentBlock != nullptr)
	{
		// if we've passed the end of the block break the loop
		if ((uptr)CurrentBlock >= BlockEnd)
			break;
		// else keep looking forward
		PreviousFreeBlock = CurrentBlock;
		CurrentBlock = CurrentBlock->Next;
	}

	if (PreviousFreeBlock == nullptr)
	{
		PreviousFreeBlock = (FreeBlock*)BlockStart;
		PreviousFreeBlock->Size = BlockSize;
		PreviousFreeBlock->Next = _FreeBlocks;

		_FreeBlocks = PreviousFreeBlock;
	}
	else if ((uptr)PreviousFreeBlock + PreviousFreeBlock->Size == BlockStart)
	{
		PreviousFreeBlock->Size += BlockSize;
	}
	else
	{
		FreeBlock* TempBlock = (FreeBlock*)BlockStart;
		TempBlock->Size = BlockSize;
		TempBlock->Next = PreviousFreeBlock->Next;

		PreviousFreeBlock->Next = TempBlock;
		PreviousFreeBlock = TempBlock;
	}

	assert(PreviousFreeBlock != nullptr);

	if ((uptr)PreviousFreeBlock + PreviousFreeBlock->Size == (uptr)PreviousFreeBlock->Next)
	{
		PreviousFreeBlock->Size += PreviousFreeBlock->Next->Size;
		PreviousFreeBlock->Next = PreviousFreeBlock->Next->Next;
	}

	_NumAllocations--;
	_UsedMemory -= BlockSize;
}
