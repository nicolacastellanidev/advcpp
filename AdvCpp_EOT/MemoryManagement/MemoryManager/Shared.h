#pragma once

namespace { // anonymous
#ifndef DEFAULT_CHUNK_SIZE
#define DEFAULT_CHUNK_SIZE 4096		// default size of a FixedAllocator Chunk - 4096 byte
#endif

#ifndef MAX_SMALL_OBJECT_SIZE
#define MAX_SMALL_OBJECT_SIZE 64	// Maximum SmallObject memory request - 64 byte
#endif

#ifndef USE_MM
#define USE_MM						// Set to TRUE to enable custom memory management
#endif

#ifndef DEBUG_LOG
#define DEBUG_LOG
#endif
}