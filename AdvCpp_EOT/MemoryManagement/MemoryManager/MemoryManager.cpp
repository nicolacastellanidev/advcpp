// MemoryManager.cpp : Defines the functions for the static library.

#include "pch.h"
#include "framework.h"
#include "MemoryManager.h"
#include "../Nica/Print.h"
#include "SmallObjectAllocator.h"
#include "FreeListAllocator.h"
#include "PointerMath.h"
#include "Shared.h"
#include "Types.h"

#include <iostream>
#include <string>
#include <cassert>
#include <cstddef>

const static Nica::String LOG_PRFX = "[MemoryManager] ";
const static WORD MM_CONSOLE_COLOR = FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_BLUE | BACKGROUND_BLUE; // white on blue bg

#ifndef MEMORY_SIZE
#define MEMORY_SIZE 1024ULL * 1024 * 1024	//1GB
#endif

#ifndef CHUNK_SIZE
#define CHUNK_SIZE 4096						// custom chunk size
#endif
/* @todo this portion of memory is always allocated, also if we don't use FLA, find a way to instantiate only when needed.
   @todo make this portion of memory dynamic
*/
void *_MEMORY = malloc(MEMORY_SIZE);

Nica::MemoryManager::MemoryManager()
	:	totMemAllocated_smallObject(0), totMemDeallocated_smallObject(0),
		totMemAllocated_bigObject(0), totMemDeallocated_bigObject(0) {

	_SmallObjectAllocator = new Nica::SmallObjectAllocator(CHUNK_SIZE, MAX_SMALL_OBJECT_SIZE);

	_FreeListAllocator = new (_MEMORY)Nica::FreeListAllocator(MEMORY_SIZE - sizeof(Nica::FreeListAllocator),
		Nica::PointerMath::add(_MEMORY, sizeof(Nica::FreeListAllocator)));
	
}

Nica::MemoryManager::~MemoryManager() {
	delete _SmallObjectAllocator;
	delete _FreeListAllocator;
}

void* Nica::MemoryManager::MM_NEW(size_t t)
{
	bool isSmall = isSmallObject(t);

	UpdateState(t, isSmall, totMemAllocated_smallObject, totMemAllocated_bigObject);

	return isSmall
		? _SmallObjectAllocator->Allocate(t)
		: _FreeListAllocator->Allocate(t, alignof(std::max_align_t));
}

void* Nica::MemoryManager::MM_NEW_A(size_t t)
{
	bool isSmall = isSmallObject(t);

	UpdateState(t, isSmall, totMemAllocated_smallObject, totMemAllocated_bigObject);

	return isSmall
		? _SmallObjectAllocator->Allocate(t)
		: _FreeListAllocator->Allocate(t, alignof(std::max_align_t));
}

void Nica::MemoryManager::MM_DELETE(void* p, size_t t)
{
	assert(p != nullptr);

	bool isSmall = isSmallObject(t);

	UpdateState(t, isSmall, totMemDeallocated_smallObject, totMemDeallocated_bigObject);
	
	isSmall
		? _SmallObjectAllocator->Deallocate(p, t)
		: _FreeListAllocator->Deallocate(p);
}

void Nica::MemoryManager::MM_DELETE_A(void* p, size_t t)
{
	assert(p != nullptr);
	bool isSmall = isSmallObject(t);

	UpdateState(t, isSmall, totMemDeallocated_smallObject, totMemDeallocated_bigObject);
	
	isSmall
		? _SmallObjectAllocator->Deallocate(p, t)
		: _FreeListAllocator->Deallocate(p);
}

void* Nica::MemoryManager::MM_MALLOC(size_t t)
{
	bool isSmall = isSmallObject(t);

	UpdateState(t, isSmall, totMemAllocated_smallObject, totMemAllocated_bigObject);

	return isSmall
		? _SmallObjectAllocator->Allocate(t)
		: malloc(t);
}

void Nica::MemoryManager::MM_FREE(void* p, size_t t)
{
	assert(p != nullptr);
	bool isSmall = isSmallObject(t);

	UpdateState(t, isSmall, totMemDeallocated_smallObject, totMemDeallocated_bigObject);
	
	isSmall
		? _SmallObjectAllocator->Deallocate(p, t)
		: free(p);
}

char Nica::MemoryManager::isSmallObject(size_t t)
{
	return t <= MAX_SMALL_OBJECT_SIZE;
}

void Nica::MemoryManager::Debug()
{
	Nica::PrintHR();
	Nica::PrintNewLine();
	Nica::PrintTitle("MemoryManager Debug Results:", FOREGROUND_GREEN);
	Nica::PrintNewLine();

	Nica::PrintSpaceBetween({ "[SMALL]", "Total memory allocated: " }, { std::to_string(totMemAllocated_smallObject), "bytes" }, MM_CONSOLE_COLOR);
	Nica::PrintSpaceBetween({ "[SMALL]", "Total memory deallocated: " }, { std::to_string(totMemDeallocated_smallObject), "bytes" }, MM_CONSOLE_COLOR);
	Nica::PrintHR();
	Nica::PrintSpaceBetween({ "[BIG]  ", "Total memory allocated: " }, { std::to_string(totMemAllocated_bigObject), "bytes" }, MM_CONSOLE_COLOR);
	Nica::PrintSpaceBetween({ "[BIG]  ", "Total memory deallocated: " }, { std::to_string(totMemDeallocated_bigObject), "bytes" }, MM_CONSOLE_COLOR);

	Nica::PrintNewLine();

	if (IsValid()) {
		Nica::PrintSpaceBetween({ "Memory leaks: " }, { "NOT DETECTED" }, FOREGROUND_GREEN);
	}
	else {
		Nica::PrintSpaceBetween({ "Memory leaks: " }, { "DETECTED" }, FOREGROUND_RED);
	}

	Nica::PrintHR();
}

void Nica::MemoryManager::Restart()
{
	totMemAllocated_smallObject = totMemDeallocated_smallObject = totMemDeallocated_bigObject = totMemAllocated_bigObject = 0;
}

char Nica::MemoryManager::IsValid()
{
	char valid = totMemAllocated_smallObject == totMemDeallocated_smallObject && totMemDeallocated_bigObject == totMemAllocated_bigObject;
	return valid;
}

char Nica::MemoryManager::IsNotValid()
{
	return !IsValid();
}

Nica::MemoryManager* Nica::MemoryManager::Get()
{
	static MemoryManager instance;
	return &instance;
}

void Nica::MemoryManager::UpdateState(size_t size, bool isSmall, size_t& smallObjectCounter, size_t& bigObjectCounter)
{
	if (isSmall) smallObjectCounter += size;
	else		 bigObjectCounter += size;
}
