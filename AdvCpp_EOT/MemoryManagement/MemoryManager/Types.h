#pragma once
#include <cstdint>
#include <string>
namespace Nica {
/**
* Custom type definitions
* @author nicola.castellani
*/
#pragma region TypeDefs
	typedef uint8_t u8;
	typedef uintptr_t uptr;
	typedef std::string String;				// typedef for std::string
	typedef std::size_t SizeType;				// typedef for std::string
#pragma endregion
}