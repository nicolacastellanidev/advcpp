#pragma once
#include "Mallocator.h"
#include "FixedAllocator.h"
#include <cstddef>s
#include <vector>

namespace Nica {

	struct CompareFixedAllocatorSize
	{
		bool operator()(const FixedAllocator& Other, std::size_t NumBytes) const;
	};

	class SmallObjectAllocator
	{
	public: 
		SmallObjectAllocator();
		SmallObjectAllocator(
			std::size_t ChunkSize,
			std::size_t MaxObjectSize);
		void* Allocate(std::size_t NumBytes);
		void Deallocate(void* p, std::size_t NumBytes);

	private:
		typedef std::vector<FixedAllocator, Mallocator<FixedAllocator>> Pool;
		Pool _Pool;
		
		FixedAllocator* _LastAllocation;
		FixedAllocator* _LastDeallocation;

		std::size_t _ChunkSize;
		std::size_t _MaxObjectSize;
	};
}