#pragma once
#include "Types.h"

namespace Nica {
	/**
	* Pointer math utilities
	* Primitive data is said to be aligned if the memory address where it is stored is a multiple of the size of the primitive.
	* A data aggregate is said to be aligned if each primitive element in the aggregate is aligned.
	* 
	* The alignment must be a power of 2!
	* 
	* @author nicola.castellani
	* @see    https://www.gamedev.net/articles/programming/general-and-gameplay-programming/c-custom-memory-allocation-r3010/
	*/
	namespace PointerMath {
		/** Adds size_t to pointer */
		inline void* add(void* p, size_t x)
		{
			return (void*)(reinterpret_cast<uptr>(p) + x);
		}
		/** Adds size_t to const pointer */
		inline const void* add(const void* p, size_t x)
		{
			return (const void*)(reinterpret_cast<uptr>(p) + x);
		}
		/** Removes size_t from pointer */
		inline void* subtract(void* p, size_t x)
		{
			return (void*)(reinterpret_cast<uptr>(p) - x);
		}
		/** Removes size_t to const pointer */
		inline const void* subtract(const void* p, size_t x)
		{
			return (const void*)(reinterpret_cast<uptr>(p) - x);
		}
		/** Returns the adjustment for alignment, 0 if already aligned */
		inline u8 alignForwardAdjustment(const void* address, u8 alignment)
		{
			// if & bitwise operator returns 0 between current address and previous alignment, we are aligned
			u8 adjustment = alignment - (reinterpret_cast<uptr>(address) & static_cast<uptr>(alignment - 1));

			if (adjustment == alignment)
				return 0; //already aligned

			return adjustment;
		}
		/** Returns the adjustment for alignment, 0 if already aligned, considering object header space */
		template<typename T>
		inline u8 alignForwardAdjustmentWithHeader(const void* address, u8 alignment)
		{
			if (alignof(T) > alignment)
				alignment = alignof(T);	// set current alignment to align of T class

			// correct eventually the gap between current alignment and next one, based on size of T
			const void* targetAlignment = add(address, sizeof(T));
			u8 adjustment = sizeof(T) + alignForwardAdjustment(targetAlignment, alignment);

			return adjustment;
		}

		inline u8 alignBackwardAdjustment(const void* address, u8 alignment)
		{
			u8 adjustment = reinterpret_cast<uptr>(address) & static_cast<uptr>(alignment - 1);

			if (adjustment == alignment)
				return 0; //already aligned

			return adjustment;
		}

		inline bool isAligned(const void* address, u8 alignment)
		{
			return alignForwardAdjustment(address, alignment) == 0;
		}

		template<class T>
		inline bool isAligned(const T* address)
		{
			return alignForwardAdjustment(address, alignof(T)) == 0;
		}

		inline void* alignForward(void* address, u8 alignment)
		{
			return (void*)((reinterpret_cast<uptr>(address) + static_cast<uptr>(alignment - 1)) & static_cast<uptr>(~(alignment - 1)));
		}

		inline const void* alignForward(const void* address, u8 alignment)
		{
			return (void*)((reinterpret_cast<uptr>(address) + static_cast<uptr>(alignment - 1)) & static_cast<uptr>(~(alignment - 1)));
		}

		inline void* alignBackward(void* address, u8 alignment)
		{
			return (void*)(reinterpret_cast<uptr>(address) & static_cast<uptr>(~(alignment - 1)));
		}

		inline const void* alignBackward(const void* address, u8 alignment)
		{
			return (void*)(reinterpret_cast<uptr>(address) & static_cast<uptr>(~(alignment - 1)));
		}
	};
}