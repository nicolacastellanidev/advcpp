#include "BaseAllocator.h"

namespace Nica {
	/**
	* Custom std::allocator using Free linked list
	* @author nicola.castellani
	*/
	class FreeListAllocator : public BaseAllocator
	{
	public:
		FreeListAllocator(): BaseAllocator(), _FreeBlocks((FreeBlock*)0x0) {}
		FreeListAllocator(size_t Size, void* Start);
		~FreeListAllocator();

		void* Allocate(size_t Size, u8 Alignment) override;
		void  Deallocate(void* Pointer) override;


	private:
		/** */
		struct AllocationHeader { size_t Size; u8 Adjustment; };
		struct FreeBlock { size_t Size; FreeBlock* Next; }; // Size is available space
		FreeListAllocator(const FreeListAllocator&);

		//Prevent copies because it might cause errors 
		FreeListAllocator& operator=(const FreeListAllocator&) {};
		FreeBlock* _FreeBlocks;
	};
}