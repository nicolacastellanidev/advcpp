#include "pch.h"
#include "FixedAllocator.h"

#include <cassert>
#include "Shared.h"

Nica::FixedAllocator::FixedAllocator(std::size_t blockSize, std::size_t chunkSize)
	: _BlockSize(blockSize), _LastAlloc(0), _LastDealloc(0)
{
	assert(_BlockSize > 0);  // prevent void allocations

	_Previous = _Next = this;	

	std::size_t numBlocks = chunkSize / blockSize;
	if (numBlocks > UCHAR_MAX) numBlocks = UCHAR_MAX;
	else if (numBlocks == 0) numBlocks = 8 * blockSize;

	_NumBlocks = static_cast<unsigned char>(numBlocks);
	assert(_NumBlocks == numBlocks);
}

Nica::FixedAllocator::FixedAllocator(const FixedAllocator& Other)
	: _BlockSize(Other._BlockSize), _NumBlocks(Other._NumBlocks), _Chunks(Other._Chunks)
{
	_Previous = &Other;
	_Next = Other._Next;
	Other._Next->_Previous = this;
	Other._Next = this;

	_LastAlloc = Other._LastAlloc
		? &_Chunks.front() + (Other._LastAlloc - &Other._Chunks.front())
		: 0;

	_LastDealloc = Other._LastDealloc
		? &_Chunks.front() + (Other._LastDealloc - &Other._Chunks.front())
		: 0;
}

Nica::FixedAllocator& Nica::FixedAllocator::operator=(const FixedAllocator& Other)
{
	FixedAllocator copy(Other);
	copy.Swap(*this);
	return *this;
}

Nica::FixedAllocator::~FixedAllocator()
{
	if (_Previous != this)
	{
		_Previous->_Next = _Next;
		_Next->_Previous = _Previous;
		return;
	}

	assert(_Previous == _Next);
	Chunks::iterator i = _Chunks.begin();
	for (; i != _Chunks.end(); ++i)
	{
		assert(i->_BlocksAvailable == _NumBlocks); // uncomment this to see memory leaks assert fail
		i->Release();
	}
}

void* Nica::FixedAllocator::Allocate()
{
	if (_LastAlloc == 0 ||
		_LastAlloc->_BlocksAvailable == 0)
	{
		// No available memory in this chunk
		// Try to find one
		Chunks::iterator i = _Chunks.begin();
		for (;; ++i)
		{
			if (i == _Chunks.end())
			{
				// All filled up-add a new chunk
				_Chunks.reserve(_Chunks.size() + 1);
				Chunk newChunk;
				newChunk.Init(_BlockSize, _NumBlocks);
				_Chunks.push_back(newChunk);
				_LastAlloc = &_Chunks.back();
				_LastDealloc = &_Chunks.back();
				break;
			}
			if (i->_BlocksAvailable > 0)
			{
				// Found a chunk
				_LastAlloc = &*i;
				break;
			}
		}
	}
	assert(_LastAlloc != 0);
	assert(_LastAlloc->_BlocksAvailable > 0);
	return _LastAlloc->Allocate(_BlockSize);
}

void Nica::FixedAllocator::Deallocate(void* Pointer)
{
	assert(!_Chunks.empty());
	assert(&_Chunks.front() <= _LastDealloc);
	assert(&_Chunks.back() >= _LastDealloc);

	_LastDealloc = VicinityFind(Pointer);
	assert(_LastDealloc);

	DoDeallocate(Pointer);
}

Nica::Chunk* Nica::FixedAllocator::VicinityFind(void* Pointer)
{
	assert(!_Chunks.empty());
	assert(_LastDealloc);

	const std::size_t chunkLength = _NumBlocks * _BlockSize;

	Chunk* Lower = _LastDealloc;
	Chunk* High = _LastDealloc + 1;
	Chunk* loBound = &_Chunks.front();
	Chunk* hiBound = &_Chunks.back() + 1;

	// Special case: deallocChunk_ is the last in the array
	if (High == hiBound) High = 0;

	for (;;)
	{
		if (Lower)
		{
			if (Pointer >= Lower->_PointerToData && Pointer < Lower->_PointerToData + chunkLength)
			{
				return Lower;
			}
			if (Lower == loBound) Lower = 0;
			else --Lower;
		}

		if (High)
		{
			if (Pointer >= High->_PointerToData && Pointer < High->_PointerToData + chunkLength)
			{
				return High;
			}
			if (++High == hiBound) High = 0;
		}
	}
	assert(false);
	return 0;
}

void Nica::FixedAllocator::DoDeallocate(void* p)
{
	assert(_LastDealloc->_PointerToData <= p);
	assert(_LastDealloc->_PointerToData + _NumBlocks * _BlockSize > p);

	// call into the chunk, will adjust the inner list but won't release memory
	_LastDealloc->Deallocate(p, _BlockSize);

	if (_LastDealloc->_BlocksAvailable == _NumBlocks)
	{
		// deallocChunk_ is completely free, should we release it? 

		Chunk& lastChunk = _Chunks.back();

		if (&lastChunk == _LastDealloc)
		{
			// check if we have two last chunks empty

			if (_Chunks.size() > 1 &&
				_LastDealloc[-1]._BlocksAvailable == _NumBlocks)
			{
				// Two free chunks, discard the last one
				lastChunk.Release();
				_Chunks.pop_back();
				_LastAlloc = _LastDealloc = &_Chunks.front();
			}
			return;
		}

		if (lastChunk._BlocksAvailable == _NumBlocks)
		{
			// Two free blocks, discard one
			lastChunk.Release();
			_Chunks.pop_back();
			_LastAlloc = _LastDealloc;
		}
		else
		{
			// move the empty chunk to the end
			std::swap(*_LastDealloc, lastChunk);
			_LastAlloc = &_Chunks.back();
		}
	}
}

std::size_t Nica::FixedAllocator::BlockSize() const
{
	return _BlockSize;
}

void Nica::FixedAllocator::Swap(FixedAllocator& rhs)
{
	std::swap(_BlockSize, rhs._BlockSize);
	std::swap(_NumBlocks, rhs._NumBlocks);
	_Chunks.swap(rhs._Chunks);
	std::swap(_LastAlloc, rhs._LastAlloc);
	std::swap(_LastDealloc, rhs._LastDealloc);
}
