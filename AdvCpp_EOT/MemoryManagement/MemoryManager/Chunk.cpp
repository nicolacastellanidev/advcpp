#include "pch.h"
#include "Chunk.h"
#include "../Nica/Print.h"
#include "MemoryManager.h"

#include <string>
#include <cassert>

void Nica::Chunk::Init(std::size_t BlockSize, unsigned char Blocks)
{
	_PointerToData = (unsigned char*)malloc(BlockSize * Blocks);	// create pointer data

	_AvailableBlock = 0;											// set default index
	_BlocksAvailable = Blocks;										// max 255

	unsigned char i = 0;
	unsigned char* p = _PointerToData;								// we use unsigned char to prevent alignment issues, like 5-byte allocation

	for (; i != Blocks; p += BlockSize)
		*p = ++i;													// allocates [blockSize] bytes of memory
}

void* Nica::Chunk::Allocate(std::size_t BlockSize)
{
	if (!_BlocksAvailable) return 0;

	unsigned char* ResultPointer = _PointerToData + (_AvailableBlock * BlockSize);	// get pointer to the last index, according to first available and offset size
	_AvailableBlock = *ResultPointer;	 											// Update firstAvailableBlock_ to point to the next block
	--_BlocksAvailable;																// decrement available blocks

	return ResultPointer;

}

void Nica::Chunk::Deallocate(void* Pointer, std::size_t BlockSize)
{
	assert(Pointer >= _PointerToData);	// cannot deallocate out-of-space bytes

	unsigned char* toRelease = static_cast<unsigned char*>(Pointer);

	assert((toRelease - _PointerToData) % BlockSize == 0); // alignment check, prevent issues due to wrong aligments

	*toRelease = _AvailableBlock;
	_AvailableBlock = static_cast<unsigned char>((toRelease - _PointerToData) / BlockSize);

	assert(_AvailableBlock == (toRelease - _PointerToData) / BlockSize); // Truncation check
	++_BlocksAvailable;
}

void Nica::Chunk::Release()
{
	free(_PointerToData);
}