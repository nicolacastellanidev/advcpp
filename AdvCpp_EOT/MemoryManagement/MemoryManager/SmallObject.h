#pragma once
#include <cstddef>
namespace Nica {
	class SmallObject
	{
	public:
		static void* operator new(std::size_t size);
		static void operator delete(void* p, std::size_t size);
		virtual ~SmallObject() {}
	};
}