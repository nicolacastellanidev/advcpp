#pragma once
#include <cstdint>
#include <cassert>
#include "Types.h"
namespace Nica {
/**
* Base allocator class
* @author nicola.castellani
*/
#pragma region BaseAllocator
	class BaseAllocator
	{
	public:
		BaseAllocator()
			: _Start(0), _Size(0), _UsedMemory(0), _NumAllocations(0) {}

		BaseAllocator(size_t Size)
			: _Start(0x0), _Size(Size)
		{
			assert(Size > 0);
			_UsedMemory = 0;
			_NumAllocations = 0;
		};

		BaseAllocator(size_t Size, void* Start)
			: _Start(Start), _Size(Size)
		{
			assert(Size > 0);
			_UsedMemory = 0;
			_NumAllocations = 0;
		};

		virtual ~BaseAllocator() {
			// memory leak detection
			assert(_NumAllocations == 0 && _UsedMemory == 0);
			_Start = nullptr;
			_Size = 0;
		}

		virtual void* Allocate(size_t Size, u8 Alignment = 4) = 0;
		virtual void Deallocate(void* Pointer) = 0;
		void*	getStart() const { return _Start; }
		size_t	getSize() const { return _Size; }
		size_t	getUsedMemory() const { return _UsedMemory; }
		size_t	getNumAllocations() const { return _NumAllocations; }

	protected:
		void* _Start;
		size_t _Size;
		size_t _UsedMemory;
		size_t _NumAllocations;
	};
#pragma endregion
}