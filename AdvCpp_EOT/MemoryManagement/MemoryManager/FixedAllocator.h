#pragma once

#include "Chunk.h"
#include "Shared.h"
#include "Mallocator.h"

#include <cstddef>
#include <vector>

namespace Nica {
	// @todo this should be moved under Types.h, but I need to include Chunk definition
	// @todo Chunk cannot be forward declared due to std::vector unknown size issue, how to solve it?

	typedef std::vector<Chunk, Mallocator<Chunk>> Chunks;

	class FixedAllocator
	{

	public:
		explicit FixedAllocator(std::size_t BlockSize = 0, std::size_t ChunkSize = DEFAULT_CHUNK_SIZE);
		FixedAllocator(const FixedAllocator& Other);
		FixedAllocator& operator=(const FixedAllocator& Other);
		~FixedAllocator();

	public:
		/**
		* Allocates requested memory
		* @author nicola.castellani
		*/
		void* Allocate();
		/**
		* Deallocates requested memory
		* @author nicola.castellani
		*/
		void Deallocate(void* Pointer);

		Chunk* VicinityFind(void* Pointer);
		std::size_t BlockSize() const;
		void DoDeallocate(void* Pointer);
		void Swap(FixedAllocator& Other);

	private:
		std::size_t _BlockSize;
		unsigned char _NumBlocks;

		Chunks _Chunks;
		Chunk* _LastAlloc;
		Chunk* _LastDealloc;

		// For ensuring proper copy semantics
		mutable const FixedAllocator* _Previous;
		mutable const FixedAllocator* _Next;
	};
}

