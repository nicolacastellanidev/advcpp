#pragma once

#include <cstddef>

namespace Nica {
	/**
	 * A chunk allocates fixed-size objects. When there is no more space, allocation returns 0.
	 */
	struct Chunk
	{
		void Init(std::size_t BlockSize, unsigned char Blocks);
		void* Allocate(std::size_t BlockSize);
		void Deallocate(void* Pointer, std::size_t BlockSize);
		void Release();

		unsigned char* _PointerToData;
		unsigned char
			_AvailableBlock,	// first available block of the chunk
			_BlocksAvailable;	// number of blocks available in chunk [max 255 block as unsigned char]
	};
}