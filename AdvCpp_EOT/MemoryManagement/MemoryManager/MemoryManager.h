﻿#pragma once
namespace Nica {

	/** Memory manager core interface */
	class IMemoryManager
	{
	public:
		/**
		* Override of operator new, calls SmallObjectAllocator or FreeListAllocator allocate method
		* @author nicola.castellani
		*/
		virtual void* MM_NEW(size_t t) = 0;
		/**
		* Override of operator new[], calls SmallObjectAllocator or FreeListAllocator allocate method
		* @author nicola.castellani
		*/
		virtual void* MM_NEW_A(size_t t) = 0;

		/**
		* Override of operator delete, calls SmallObjectAllocator or FreeListAllocator deallocate method
		* @author nicola.castellani
		*/
		virtual void  MM_DELETE(void* p, size_t t) = 0;
		/**
		* Override of operator delete[], calls SmallObjectAllocator or FreeListAllocator deallocate method
		* @author nicola.castellani
		*/
		virtual void  MM_DELETE_A(void* p, size_t t) = 0;

		/**
		* Override of operator malloc, calls SmallObjectAllocator or FreeListAllocator allocate method
		* @author nicola.castellani
		*/
		virtual void* MM_MALLOC(size_t t) = 0;
		/**
		* Override of operator free, calls SmallObjectAllocator or FreeListAllocator deallocate method
		* @author nicola.castellani
		*/
		virtual void  MM_FREE(void* p, size_t t) = 0;
	};

	class MemoryManager : public IMemoryManager
	{
	private:
		/** Debug properties */
		size_t totMemAllocated_smallObject;
		size_t totMemAllocated_bigObject;
		size_t totMemDeallocated_smallObject;
		size_t totMemDeallocated_bigObject;

	public:
		MemoryManager();
		virtual ~MemoryManager();

		virtual void* MM_NEW(size_t t);
		virtual void* MM_NEW_A(size_t t);
		virtual void* MM_MALLOC(size_t t);

		virtual void  MM_DELETE(void* p, size_t t);
		virtual void  MM_DELETE_A(void* p, size_t t);
		virtual void  MM_FREE(void* p, size_t t);

		/**
		* returns true if size is greater than MAX_SMALL_OBJECT_SIZE (4096bytes)
		* @author nicola.castellani
		*/
		char isSmallObject(size_t t);

		/**
		* Prints a debug informations about memory manager
		* @author nicola.castellani
		*/
		void Debug();

		/**
		* Clears internal state
		* @author nicola.castellani
		*/
		void Restart();

		/**
		* Returns true if total memory allocated is equal to deallocated
		* @author nicola.castellani
		*/
		char IsValid();
		/**
		* Returns true if total memory allocated is not equal to deallocated
		* @author nicola.castellani
		*/
		char IsNotValid();
	
	public:
		/**
		* Singleton pattern implementation
		* @todo create a more complex implementation
		* @author nicola.castellani
		*/
		static MemoryManager* Get();

	private:
		/** 
		* [FWD] Allocates only small objects
		* @author nicola.castellani
		*/
		class SmallObjectAllocator* _SmallObjectAllocator;
		/**
		* [FWD] Allocates only big objects
		* @author nicola.castellani
		*/
		class FreeListAllocator* _FreeListAllocator;

		void UpdateState(size_t size, bool isSmall, size_t& smallObjectCounter, size_t& bigObjectCounter);
	};
}