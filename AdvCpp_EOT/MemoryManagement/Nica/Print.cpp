#include "pch.h"

#include "Print.h"
#include <string>

const char SPECIAL_CHAR = '-';
const char SPECIAL_CHAR_TITLE = '=';
const char EMPTY_CHAR = ' ';

typedef unsigned int UINT;
const UINT ROW_LEN = 74;
HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

template<class U>
Nica::String Nica::PrintPair(Nica::String prefix, const U value, WORD consoleTextAttr)
{
	SetConsoleAttribute(consoleTextAttr);
	return prefix + ": " + std::to_string(value);
	SetConsoleAttribute();
}

template<>
Nica::String Nica::PrintPair(Nica::String prefix, const double value, WORD consoleTextAttr)
{
	SetConsoleAttribute(consoleTextAttr);
	return prefix + ": " + std::to_string(value);
	SetConsoleAttribute();
}

void Nica::PrintHR()
{
	SetConsoleAttribute();
	String result;

	for (UINT i = 0; i < ROW_LEN; i++) {
		result += SPECIAL_CHAR;
	}

	std::cout << result << '\n';
}

void Nica::PrintSpaceBetween(StringArray left, StringArray right, WORD consoleTextAttr)
{
	SetConsoleAttribute(consoleTextAttr);
	Nica::String result;

	for (UINT i = 0; i < left.size(); i++)
	{
		result += left[i] + ' ';
	}

	UINT rightWordsSize = 0;

	for (UINT i = 0; i < right.size(); i++)
	{
		rightWordsSize += right[i].size() + 1; // add 1 for space
	}
	rightWordsSize -= 1; // remove last space

	for (UINT i = result.size(); i < ROW_LEN - rightWordsSize; i++)
		result += EMPTY_CHAR;

	for (UINT i = 0; i < right.size(); i++)
		result += right[i] + ' ';

	std::cout << result;

	PrintNewLine();
}

void Nica::PrintTitle(String title, WORD consoleTextAttr, UINT padding)
{
	PrintHR();
	SetConsoleAttribute(consoleTextAttr);
	
	UINT titleLen = title.size();
	char isOdd = titleLen % 2 != 0;
	Nica::String result;
	UINT center = ROW_LEN / 2;
	UINT lastIndex = center - titleLen / 2;


	for (UINT i = 0; i < lastIndex; i++) {
		result += (i < lastIndex - padding) ? SPECIAL_CHAR_TITLE : ' ';
	}

	result.append(title);

	lastIndex = ROW_LEN - (isOdd ? 1 : 0);

	for (UINT i = center + titleLen/2; i < lastIndex; i++) {
		result += (i > center + titleLen / 2 + padding) ? SPECIAL_CHAR_TITLE : ' ';
	}

	std::cout << result;
	PrintNewLine();
	SetConsoleAttribute();
	PrintHR();
}

void Nica::PrintNewLine()
{
	SetConsoleAttribute();
	std::cout << '\n';
}

void Nica::SetConsoleAttribute(WORD consoleTextAttr)
{
	SetConsoleTextAttribute(hConsole, consoleTextAttr);
}
