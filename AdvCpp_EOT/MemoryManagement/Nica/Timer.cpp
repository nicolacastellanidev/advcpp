#include "pch.h"
#include "Timer.h"
#include <ctime>
#include <chrono>

using std::chrono::duration_cast;
using std::chrono::milliseconds;
using std::chrono::seconds;
using std::chrono::system_clock;

Nica::Timer* Nica::Timer::get()
{
    static Timer instance;
    return &instance;
}

void Nica::Timer::start()
{
    start_millisec = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
}

unsigned int Nica::Timer::end()
{
	end_millisec = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
	auto diff = end_millisec - start_millisec;
    return diff;
}
