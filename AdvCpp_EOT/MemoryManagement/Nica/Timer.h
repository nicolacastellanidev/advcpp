#pragma once

namespace Nica {
	class Timer
	{

	public:
		static Timer* get();

	public:
		void start();
		unsigned int end();

	private:
		unsigned int start_millisec;
		unsigned int end_millisec;
	};
}