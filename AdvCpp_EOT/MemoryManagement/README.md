# MemoryManagement

This exercise consists of a set of custom memory allocators:

1. ***SmallObjectAllocator*** (an implementation based on Andrei Alexandrescu **SmallObj** class)
2. ***FreeListAllocator***: a custom implementation of a free list based memory management.

---
## Before getting started
You can test the project running the **main.cpp** file. ***In Debug Mode the run will end with some assert fails, due to the final tests about memory leaks***.

---

# Documentation

### `DynamicAllocationObject`

A **DynamicAllocationObject** uses my custom **MemoryManager** as core allocator.

The class overrides **core operators** new/delete:

```c++
static void* operator new(std::size_t size);
static void operator delete(void* p, std::size_t size);

static void* operator new[](std::size_t size);
static void operator delete[](void* p, std::size_t size);

static void* malloc(std::size_t size);
static void  free(void* p);
```

and these operators calls relative **MemoryManager** functions, for example:
```c++
#ifdef USE_MM
	return Nica::MemoryManager::Get()->MM_NEW(size);
#else
	return ::operator new(size);
#endif
```

---

### `MemoryManager`

The **MemoryManager** handles allocation and deallocation requests, checking which allocator to use for the requested size:

```c++
return isSmall
		? _SmallObjectAllocator->allocate(t)
		: _FreeListAllocator->allocate(t, alignof(std::max_align_t));
```

An object is considered **small** if its size is less or equal of ***MAX_SMALL_OBJECT_SIZE*** (*64 bytes by default*):

```c++
char Nica::MemoryManager::isSmallObject(size_t t)
{
	return t <= MAX_SMALL_OBJECT_SIZE;
}
```

It also keeps **internal stats** about the amount of memory allocated and deallocated, for test purposes.

---
## Small Object Allocation

### `SmallObjectAllocator`
This allocator keeps a **pool** of fixed allocated objects (**FixedAllocator**), and keeps track of the last allocated and deallocated object.

### `FixedAllocator`
This object allocates a vector of **Chunks**, fixed-size objects (**_chunkSize**), to allocate the requested size (**_blockSize**).

A chunk is filled until it hasn't enough space to allocate memory, in that case the FixedAllocator requests a new Chunk and start to fill it using fixed size blocks of memory.

It also keep track of next and previous FixedAllocator, the number of blocks created and the last allocated and deallocated chunk;

### `Chunk`
A chunk allocates fixed-size objects. When there is no more space, allocation returns 0.

It keeps track of the first available block and how many blocks are still available.

---
## Big Object Allocation

### `BaseAllocator`

Core class for custom allocators, exposes some useful methods and attributes to child class.

### `FreeListAllocator`

Implementation of BaseAllocator, handles memory allocation using a free linked list.

A block of a list is composed by an header (**AllocationHeader**), which keeps track of informations like the block size and the adjustment for the aligment.

### `CustomSTLAllocator`

Custom STL compatible allocator, uses **MemoryManager** for allocation.  

***`see`*** [STL Allocator](https://en.cppreference.com/w/cpp/memory/allocator)

### `Mallocator`

This allocator uses ***malloc*** and ***free*** method to skip custom allocations. This is useful to prevent infinite loops while allocating FixedAllocator objects. 

---
## References
* [Loki SmallObj class](https://github.com/snaewe/loki-lib/blob/master/include/loki/SmallObj.h)
* [STL Allocator](https://en.cppreference.com/w/cpp/memory/allocator)