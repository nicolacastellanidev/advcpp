#pragma once
#include <cstddef>
namespace Nica {
	/**
	* A DynamicAllocationObject overrides default allocation and deallocation methods using MemoryManager
	* @author nicola.castellani
	*/
	class DynamicAllocationObject
	{
	public:
		virtual ~DynamicAllocationObject() {}

		static void* operator new(std::size_t size);
		static void operator delete(void* p, std::size_t size);

		static void* operator new[](std::size_t size);
		static void operator delete[](void* p, std::size_t size);

		static void* malloc(std::size_t size);
		static void  free(void* p);
	};
}

