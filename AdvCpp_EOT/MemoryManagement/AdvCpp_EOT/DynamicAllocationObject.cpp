#include "DynamicAllocationObject.h"
#include "../MemoryManager/MemoryManager.h"
#include "../MemoryManager/Shared.h"
#include "../Nica/Print.h"
#include <cstdlib>

void* Nica::DynamicAllocationObject::operator new(std::size_t size)
{
#ifdef USE_MM
	return Nica::MemoryManager::Get()->MM_NEW(size);
#else
	return ::operator new(size);
#endif
}

void Nica::DynamicAllocationObject::operator delete(void* p, std::size_t size)
{
#ifdef USE_MM
	Nica::MemoryManager::Get()->MM_DELETE(p, size);
#else
	::operator delete(p, size);
#endif
}

void* Nica::DynamicAllocationObject::operator new[](std::size_t size)
{
#ifdef USE_MM
	return Nica::MemoryManager::Get()->MM_NEW_A(size);
#else
	return ::operator new(size);
#endif
}

void Nica::DynamicAllocationObject::operator delete[](void* p, std::size_t size)
{
#ifdef USE_MM
	Nica::MemoryManager::Get()->MM_DELETE_A(p, size);
#else
	::operator delete(p, size);
#endif
}

void* Nica::DynamicAllocationObject::malloc(std::size_t size)
{
#ifdef USE_MM
	return Nica::MemoryManager::Get()->MM_MALLOC(size);
#else
	return std::malloc(size);
#endif
}

void Nica::DynamicAllocationObject::free(void* p)
{
#ifdef USE_MM
	Nica::MemoryManager::Get()->MM_FREE(p, 0);
#else
	std::free(p);
#endif
}
