// AdvCpp_EOT.cpp : This file contains the 'main' function. Program execution begins and ends there.
#include "../MemoryManager/SmallObject.h"
#include "../Nica/Print.h"
#include "../MemoryManager/MemoryManager.h"
#include "../Nica/Timer.h"
#include "../MemoryManager/Shared.h"
#include "DynamicAllocationObject.h"
#include "../MemoryManager/CustomSTLAllocator.h"
#include "../MemoryManager/Types.h"

#include <iostream>
#include <string>
#include <cassert>
#include <list>

/**
* -----------------------------------------------------------
* | Custom memory management using SmallObject allocator,	|
  | Free List allocator and a STL compatible allocator.		|	
  |															|
* |	@author  nicola.castellani								|
* | @version 1.0.0											|
* -----------------------------------------------------------
*/

#pragma region Typedefs and locals
const Nica::String LOG_PRFX = "[Main] ";		// core log prefix for main

const char TITLE_STYLE = FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED;

/**
* Custom non-fixed-size list with CustomSTLAllocator
* @author nicola.castellani
*/
template<class T, class A = Nica::CustomSTLAllocator<T>>
class CustomSTLAllocatorList : public std::list<T, A> {};

#pragma endregion

#pragma region Classes
/**
* Complex numbers class, SmallObject
* @author nicola.castellani
*/
class Complex : public Nica::DynamicAllocationObject // 16 byte from double, 8 byte virtual table, 8 byte alignment
{
public:
	Complex() : r(0), c(0) {};
	Complex(double a, double b) : r(a), c(b) {}
private:
	double r; // Real Part
	double c; // Complex Part
};
/**
* Simple big object class, extends a class with custom allocators set
* @author nicola.castellani
*/
class BigObjectExample : public Nica::DynamicAllocationObject // 72 byte + 8 byte virtual destructor + 8 byte header, 8 byte alignment
{
public:
	BigObjectExample()
		: a(0), b(0), c(0), d(0), e(0), f(0), g(0), h(0), i(0) {}
	BigObjectExample(double a, double b, double c, double d, double e, double f, double g, double h, double i)
		: a(a), b(b), c(c), d(d), e(e), f(f), g(g), h(h), i(i) {}
private:
	double a, b, c, d, e, f, g, h, i; // 8byte x 9  = 72 byte
};

#pragma endregion

#pragma region Tests

void Check(bool valid) {
	Nica::PrintSpaceBetween({ "Test" }, { valid ? "Passed" : "Failed" }, valid ? BACKGROUND_GREEN : BACKGROUND_RED);
}

/**
* Instantiate Complex 10mln times and check if there are mem leaks
* @author nicola.castellani
*/
void CheckComplexAllocation() {
	Nica::PrintHR();
	Complex* array[1000];

	Nica::Timer::get()->start();

	for (int i = 0; i < 5000; i++) {
		for (int j = 0; j < 1000; j++) {
			array[j] = new Complex(i, j);
		}
		for (int j = 0; j < 1000; j++) {
			delete array[j];
		}
	}

	Nica::PrintSpaceBetween({ "Total execution time:" }, { std::to_string((float)Nica::Timer::get()->end() / 1000), "seconds" }, FOREGROUND_GREEN | FOREGROUND_RED);

	Nica::MemoryManager::Get()->Debug();
	Check(Nica::MemoryManager::Get()->IsValid());
	Nica::MemoryManager::Get()->Restart();
}

/**
* Creates dangling pointer
* @author nicola.castellani
*/
void CreateMemoryLeak() {
	Complex* ptr = new Complex(0, 0);
	return; // ptr is not deleted
}
/**
* Tests memory leaks handling
* @author nicola.castellani
*/
void CheckMemoryLeak() {
	CreateMemoryLeak();
	Nica::MemoryManager::Get()->Debug();
	Check(Nica::MemoryManager::Get()->IsNotValid());
	Nica::MemoryManager::Get()->Restart();
}

/**
* Creates dangling pointer for FreeList
* @author nicola.castellani
*/
void CreateFLMemoryLeak() {
	BigObjectExample* ptr = new BigObjectExample(0,0,0,0,0,0,0,0,0);
	return; // ptr is not deleted
}

/**
* Tests memory leaks handling with free list
* @author nicola.castellani
*/
void CheckFLMemoryLeak() {
	CreateFLMemoryLeak();
	Nica::MemoryManager::Get()->Debug();
	Check(Nica::MemoryManager::Get()->IsNotValid());
	Nica::MemoryManager::Get()->Restart();
}

/**
* Tests big objects instantiation and destruction
* @author nicola.castellani
*/
void CheckBigObjects() {
	BigObjectExample* array[10];

	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 10; j++) {
			array[j] = new BigObjectExample{ 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		}
		for (int j = 0; j < 10; j++) {
			delete array[j];
		}
	}

	Nica::MemoryManager::Get()->Debug();
	Check(Nica::MemoryManager::Get()->IsValid());
	Nica::MemoryManager::Get()->Restart();
}

/**
* Creates an arbitrary CreateCustomSTLAllocatorList with Complex, and tests destruction
* @author nicola.castellani
*/
void CreateCustomSTLAllocatorListAndDestroy() {
	CustomSTLAllocatorList<Complex*> test{};

	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 10; j++) {
			test.push_back(new Complex(0, j));
		}
		for (int j = 0; j < 10; j++) {
			// get current element
			// delete element
			delete test.front();
			test.pop_front(); // this deletes and removes element
		}
	}

	// delete test;
}

/**
* Creates an empty CustomSTLAllocatorList
* @author nicola.castellani
*/
void CreateCustomSTLAllocatorListWithLeak() {
	CustomSTLAllocatorList<Complex*> test{};
	test.push_back(new Complex(0, 0));
}

/**
* Tests instantiation with custom STL Allocator
* @author nicola.castellani
*/
void CheckSTLAllocator() {
	CreateCustomSTLAllocatorListAndDestroy();
	Nica::MemoryManager::Get()->Debug();
	Check(Nica::MemoryManager::Get()->IsValid());
	Nica::MemoryManager::Get()->Restart();
}

/**
* Tests instantiation with custom STL Allocator, and memory leak check
* @author nicola.castellani
*/
void CheckSTLAllocatorLeak() {
	CreateCustomSTLAllocatorListWithLeak();

	Nica::MemoryManager::Get()->Debug();
	Check(Nica::MemoryManager::Get()->IsNotValid());
	Nica::MemoryManager::Get()->Restart();
}


#pragma endregion

int main()
{
	/* Small Object Allocator */
	Nica::PrintTitle("TEST 1 >>> Check Complex class allocation", TITLE_STYLE);
	CheckComplexAllocation();

	Nica::PrintTitle("TEST 2 >>> Check Complex class allocation leak detection", TITLE_STYLE);
	CheckMemoryLeak();
	
	/* Free List Allocator */
	Nica::PrintTitle("TEST 3 >>> Check big object class allocation", TITLE_STYLE);
	CheckBigObjects();
	
	Nica::PrintTitle("TEST 4 >>> Check big object class allocation leaks", TITLE_STYLE);
	CheckFLMemoryLeak();
	
	/* Custom STL Allocator */
	Nica::PrintTitle("TEST 5 >>> Check custom STL allocation", TITLE_STYLE);
	CheckSTLAllocator();
	
	Nica::PrintTitle("TEST 6 >>> Check custom STL allocation leak detection", TITLE_STYLE);
	CheckSTLAllocatorLeak();

	return 0;
}