#include "BigInteger.h"
#include <algorithm>
#include <iostream>
#include <vector>

#define M_PI 3.14159265358979323846 /* pi */

typedef std::vector<BigInteger> BigIntegerVec;

#pragma region CTORS
BigInteger::BigInteger()
{
	Reset();
}
BigInteger::BigInteger(size_t Value)
{
	SetFrom(Value);
}
BigInteger::BigInteger(int Value)
{
	SetFrom(Value);
}
BigInteger::BigInteger(double Value)
{
	SetFrom(Value);
}
BigInteger::BigInteger(float Value)
{
	SetFrom(Value);
}
BigInteger::BigInteger(long Value)
{
	SetFrom(Value);
}
BigInteger::BigInteger(long long Value)
{
	SetFrom(Value);
}
BigInteger::BigInteger(std::string Value)
{
	SetFrom(Value);
}
#pragma endregion

#pragma region Utilities
void BigInteger::Reset()
{
	Digits.clear();
	Sign = 0;
}
void BigInteger::Trim()
{
	auto iter1 = Digits.rbegin();

	while (!Digits.empty() && (*iter1) == 0) {
		Digits.pop_back();
		iter1 = Digits.rbegin();
	}

	if (Digits.size() == 0) {
		Sign = 0;
		Digits.push_back(0);
	}
}
BigInteger abs(const BigInteger& Value) {
	return Value.IsNegative() ? Value * -1 : Value;
}
void BigInteger::SetFrom(INTEGER_DIGIT_T_MAX Value)
{
	Reset();
	if (Value == 0) {
		Digits.push_back(Value);
		return;
	}
	if (Value < 0) {
		Sign = 1;
		Value *= -1;
	}
	SplitNumberIntoDigits(Value);
}

void BigInteger::SplitNumberIntoDigits(INTEGER_DIGIT_T_MAX Value)
{
	if (Value == 0) {
		Digits.push_back(0);
		return;
	}

	do {
		Digits.push_back(Value % BASE);
		Value /= BASE;
	} while (Value > 0);

	Trim();
}

void BigInteger::SetFrom(std::string Value)
{
	Reset();

	if (Value.length() == 0)
	{
		Sign = 0;
		return;
	}

	if (Value[0] == '-')
	{
		Sign = 1; // Sign
	}

	size_t Iteration = 0;
	for (size_t i = 0; i < Value.size(); i += 1) {
		size_t Limit = Value.size() < Iteration + 1 ? Value.size() : 1;
		Digits.push_front(std::stoul(Value.substr(Iteration++ * 1, Limit)));
	}

	Trim();
}
std::string BigInteger::ToString() const
{
	if (Digits.size() == 0) return "0";

	std::string result = Sign ? "-" : "";

	for (auto iter = Digits.rbegin(); iter != Digits.rend(); iter++)
		result.append(std::to_string(*iter));

	return result;
}
char BigInteger::IsNegative() const
{
	return Sign == 1;
}
BigInteger BigInteger::Sum(const BigInteger& Other) const
{
	if (Other.Digits.size() > Digits.size() || Other.IsAbsGreater(*this)) {
		return Other.Sum(*this);
	}

	BigInteger Result;

	int OperationSign = Sign != Other.Sign ? -1 : 1;
	int Carry = 0;
	
	Result.Sign = Other.IsAbsGreater(*this, 1) ? Other.Sign : Sign;

	for (size_t i = 0; i < std::max<size_t>(Digits.size(), Other.Digits.size()); ++i) {
		int SumResult = 0;

		if (i < Digits.size()) SumResult += Digits[i] + Carry * OperationSign;

		if (i < Other.Digits.size()) SumResult += Other.Digits[i] * OperationSign;

		if (Carry > 0) Carry--;

		if (SumResult < 0) {
			SumResult += BASE;
			Carry++;
		}

		if (SumResult >= BASE) {
			SumResult -= BASE;
			Carry++;
		}

		Result.SplitNumberIntoDigits(SumResult);
	}
	if (Carry) Result.SplitNumberIntoDigits(Carry);

	Result.Trim();

	return Result;
}
BigInteger BigInteger::Multiply(const BigInteger& Other) const
{
	// quick checks
	if (!*this || !Other) {    // if multiplying by 0
		return 0;
	}

	if (*this == 0 || Other == 0) {
		return 0;
	}

	if (*this == 1) {			// if multiplying by 1
		return Other;
	}

	if (Other == 1) {          // if multiplying by 1
		return *this;
	}

	BigInteger Result;
	BigInteger PartialResult;
	BigIntegerVec Partials;

	INTEGER_DIGIT_T Carry = 0;

	for (size_t i = 0; i < Other.Digits.size(); ++i) {

		// first, multiply each digit of second number to each of the first
		INTEGER_DIGIT_T ValueB = Other.Digits[i];
		PartialResult.Reset();

		// prepend 0 for dozens and hundreds and over
		for (size_t u = 0; u < i; u++) PartialResult.Digits.push_front(0); 

		for (size_t j = 0; j < Digits.size(); ++j) {
			INTEGER_DIGIT_T ValueA = Digits[j];
			INTEGER_DIGIT_T ValueBxValueA = ValueB * ValueA;

			// sum the carry from the previous operation 
			if (Carry) {
				ValueBxValueA += Carry;
				Carry = 0;
			}

			// check if the result is greater than BASE
			if (ValueBxValueA >= BASE) {
				Carry = ValueBxValueA / BASE;
				ValueBxValueA = ValueBxValueA - (ValueBxValueA / BASE) * BASE;
			}

			PartialResult.Digits.push_back(ValueBxValueA);
		}

		if (Carry) {
			PartialResult.Digits.push_back(Carry);
			Carry = 0;
		}

		Partials.push_back(PartialResult);
	}

	// sum all the partials to get the final result
	for (size_t i = 0; i < Partials.size(); ++i) Result += Partials[i];

	return Result;
}
BigInteger BigInteger::Divide(const BigInteger& Other) const
{
	// quick checks
	if (!*this || *this == 1) {
		return BigInteger(0);
	}

	if (!Other) {    // if multiplying by 0
		return 0;
	}

	if (*this == 0 || Other == 0) {
		return 0;
	}

	if (*this == 1) {			// if multiplying by 1
		return 0;
	}

	if (Other == 1) {          // if multiplying by 1
		return *this;
	}

	if (Other.IsAbsGreater(*this)) {
		return BigInteger(0);
	}

	BigInteger _Other = BigInteger{ Other };
	if (_Other.IsNegative()) _Other.Sign = 0;

	BigInteger Result;
	Result.Sign = Sign ^ Other.Sign;

	// first, get the minimum number greather than other

	BigInteger DividendPart;
	int LastIndex = Digits.size() - 1;

	for (LastIndex; LastIndex >= 0; --LastIndex) {
		DividendPart.Digits.push_front(Digits[LastIndex]);
		if (DividendPart >= _Other) {
			// then, find the minimum multiplier for Other to be less or equals to the dividend part
			size_t i = 2;
			BigInteger BiggestLowerInt{ _Other };
			// https://ilgeniodellamatematica.altervista.org/divisione-in-colonna/
			while (i) {
				BiggestLowerInt = _Other * i;
				if (BiggestLowerInt == DividendPart) {
					Result.Digits.push_front(i);
					break;
				}
				if (BiggestLowerInt.IsAbsGreater(DividendPart)) {
					BiggestLowerInt = _Other * (i - 1);
					Result.Digits.push_front(i - 1);
					break;
				}
				++i;
			}

			// after that, find the rest
			BigInteger Rest = DividendPart - BiggestLowerInt;
			DividendPart = BigInteger(Rest);
		}
	}

	return Result;
}
BigInteger pow(const BigInteger& Value, unsigned int Exp)
{
	if (Exp == 0) {
		if (Value == 0)
			throw std::logic_error("Zero cannot be raised to zero");
		return 1;
	}

	BigInteger Result = Value, ResultOdd = 1;
	while (Exp > 1) {
		if (Exp % 2)
			ResultOdd *= Result;
		Result *= Result;
		Exp /= 2;
	}

	return (Result * ResultOdd);
}
#pragma endregion

#pragma region Operators overrides
void BigInteger::operator*=(const BigInteger& Other)
{
	*this = BigInteger{ *this * Other };
}
BigInteger BigInteger::operator*(const BigInteger& Other) const
{
	BigInteger Result = Multiply(Other);
	Result.Sign = Sign ^ Other.Sign;
	return Result;
}
BigInteger BigInteger::operator*(const size_t Other) const
{
	return *this * BigInteger(Other);
}
void BigInteger::operator/=(const BigInteger& Other)
{
	*this = BigInteger{ Divide(Other) };
}
BigInteger BigInteger::operator/(const BigInteger& Other) const
{
	BigInteger Result = Divide(Other);
	return Result;
}
BigInteger BigInteger::operator+(const BigInteger& Other)
{
	return Sum(Other);
}
BigInteger BigInteger::operator-(const BigInteger& Other)
{
	BigInteger _Other = Other;
	_Other.Sign = 1 - Other.Sign; // swap sign
	return Sum(_Other);
}
BigInteger BigInteger::operator-()
{
	Sign = 1 - Sign;
	return *this;
}
void BigInteger::operator+=(const BigInteger& Other)
{
	BigInteger Result = Sum(Other);
	Sign = Result.Sign;
	Digits = Result.Digits;
}
void BigInteger::operator-=(const BigInteger& Other)
{
	BigInteger _Other = Other;
	_Other.Sign = 1 - Other.Sign; // swap sign
	*this += _Other;
}
bool BigInteger::operator!() const
{
	return this == nullptr || Digits.size() == 0 || (Digits.size() == 1 && Digits[0] == 0);
}
bool BigInteger::operator==(const BigInteger& Other) const {
	return ((Sign == Other.Sign) && (Digits == Other.Digits));
}
char BigInteger::IsAbsGreater(const BigInteger& Other, char CheckEquality) const
{
	if (Other.Digits.size() < Digits.size()) return 1;
	if (Other.Digits.size() > Digits.size()) return 0;

	char AreEquals = 1;
	
	for (int i = Digits.size() - 1; i >= 0; --i) {
		if (CheckEquality && abs(Digits[i]) <= abs(Other.Digits[i])) {
			return 0;
		}
		else if (abs(Digits[i]) < abs(Other.Digits[i])) return 0;
		else if (AreEquals && abs(Digits[i]) != abs(Other.Digits[i])) AreEquals = 0;
	}
	
	return CheckEquality ? AreEquals : !AreEquals;
}
bool BigInteger::operator>=(const BigInteger& Other) const
{
	if (IsNegative() && !Other.IsNegative()) return false;
	if (Other.IsNegative() && !IsNegative()) return true;
	if (Other.Digits.size() < Digits.size()) return true;
	if (Other.Digits.size() > Digits.size()) return false;
	for (int i = Digits.size() - 1; i >= 0; --i) {
		if (Digits[i] > Other.Digits[i]) return true;
	}
	return false;
}
bool BigInteger::operator>(const BigInteger& Other) const
{
	if (IsNegative() && !Other.IsNegative()) return false;
	if (Other.IsNegative() && !IsNegative()) return true;
	if (Other.Digits.size() < Digits.size()) return true;
	if (Other.Digits.size() > Digits.size()) return false;
	for (int i = Digits.size() - 1; i >= 0; --i) {
		if (Digits[i] < Other.Digits[i]) return false;
	}
	return true;
}
bool BigInteger::operator!=(const BigInteger& Other) const {
	return !(*this == Other);
}
#pragma endregion