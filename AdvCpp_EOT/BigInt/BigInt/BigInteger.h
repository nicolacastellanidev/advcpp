#pragma once
#include <deque>
#include <string>

#ifndef INTEGER_DIGIT_T
#define INTEGER_DIGIT_T				int8_t	// max 32, use 64 as the largest one
#endif

#ifndef INTEGER_DIGIT_T_MAX
#define INTEGER_DIGIT_T_MAX			int64_t	// max 32, use 64 as the largest one
#endif

#ifndef BASE
#define BASE 10
#endif

class BigInteger
{
public:

	BigInteger();
	BigInteger(size_t Value);
	BigInteger(int Value);
	BigInteger(double Value);
	BigInteger(float Value);
	BigInteger(long Value);
	BigInteger(long long Value);
	BigInteger(std::string Value);

	BigInteger operator*(const BigInteger& Other) const;
	BigInteger operator*(const size_t Other) const;
	BigInteger operator/(const BigInteger& Other) const;
	BigInteger operator+(const BigInteger& Other);
	BigInteger operator-(const BigInteger& Other);
	BigInteger operator-();

	void operator*=(const BigInteger& Other);
	void operator/=(const BigInteger& Other);
	void operator+=(const BigInteger& Other);
	void operator-=(const BigInteger& Other);
	bool operator!() const;
	bool operator!=(const BigInteger& Other) const;
	bool operator==(const BigInteger& Other) const;
	bool operator>=(const BigInteger& Other) const;
	bool operator>(const BigInteger& Other) const;

	/**
	* Converts BigInteger to string format
	* @author nicola.castellani
	*/
	std::string ToString() const;
	/**
	* Check if number is negative
	* @author nicola.castellani
	* @return true if Sign == 0
	*/
	char IsNegative() const;
	/**
	* Check equality between big integers
	* @author nicola.castellani
	* @return true if BigInteger is absolutely greather than BigInteger,
	* also equals if CheckEquality is set to true
	*/
	char IsAbsGreater(const BigInteger& Other, char CheckEquality = 0) const;
	/**
	* Trims out repeating 0 at the start of the number
	* @author nicola.castellani
	*/
	void Trim();
	/**
	* Array of digits, each digit is saved inside a deque cell
	* @author nicola.castellani
	*/
	std::deque<INTEGER_DIGIT_T> Digits;
private:

	INTEGER_DIGIT_T Tail;				// last value
	INTEGER_DIGIT_T_MAX Exceeds = 0;	// Exceeded bytes

	char Sign;							// 0 positive | 1 negative

// arithmetic functions
private:
	/**
	* Sum 2 biginteger with an eye on NEG1 limit
	* @author nicola.castellani
	*/
	BigInteger Sum(const BigInteger& Other) const;

	/**
	* Multiply 2 biginteger with an eye on NEG1 limit
	* @author nicola.castellani
	*/
	BigInteger Multiply(const BigInteger& Other) const;

	/**
	* Divide 2 biginteger with an eye on NEG1 limit
	* @author nicola.castellani
	*/
	BigInteger Divide(const BigInteger& Other) const;

	/**
	* Reset deque and exceeds
	* @author nicola.castellani
	*/
	void Reset();

private:
	/**
	* Creates a BigInteger from a numeric value
	*  @author nicola.castellani
	*/
	void SetFrom(INTEGER_DIGIT_T_MAX Value);
	/**
	* Creates a BigInteger from a string value
	*  @author nicola.castellani
	*/
	void SetFrom(std::string Value);
	/**
	* Split each Digit from numeric values
	* @author nicola.castellani
	*/
	void SplitNumberIntoDigits(INTEGER_DIGIT_T_MAX Value);
};


BigInteger pow(const BigInteger& Value, unsigned int Exp);

