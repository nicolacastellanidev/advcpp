﻿#pragma once
#include "BigInteger.h"
#include "../../MemoryManagement/Nica/Print.h"
#include <iostream>
#include <string>

namespace BigIntSpecs {
// --- remove to enable debug logs ---
// #ifndef DEBUG
// #define DEBUG
// #endif

	char Check(BigInteger Original, BigInteger Expected, std::string Title) {
		char Result = Original == Expected;
		if (Result) {
			Nica::PrintSpaceBetween({ Title }, { "[:)] [Test Passed]" }, FOREGROUND_GREEN);
			return Result;
		}
		Nica::PrintSpaceBetween(
			{ Title },
			{ "[:(] [Test Failed]" }, FOREGROUND_RED);
#ifdef DEBUG
		Nica::PrintSpaceBetween(
			{ "\nOriginal [ " + Original.ToString() + " ]" },
			{ "Expected [ " + Expected.ToString() + " ]\n" }, FOREGROUND_GREEN | FOREGROUND_RED);
#endif // DEBUG

		return Result;
	}

#pragma region CTOR tests
	/**
	* Tests default constructor
	*/
	__forceinline char TestDefaultCTOR() {
		BigInteger a = 1024;
		return Check(a, BigInteger(512) + BigInteger(512), "[ TestDefaultCTOR ]")
			&& Check(BigInteger(1000000), BigInteger(1000000), "[ TestDefaultCTOR [1000000] ]");
	}

	/**
	* Tests default constructor with negative numbers
	*/
	__forceinline char TestDefaultCTORWithNegative() {
		BigInteger a = -1024;
		return Check(a, BigInteger(-512) + BigInteger(-512), "[ TestDefaultCTORWithNegative ]");
	}

	/**
	* Tests default constructor with negative numbers
	*/
	__forceinline char TestCopyCTOR() {
		BigInteger a = -1024;
		BigInteger b = a;
		return Check(b, a, "[ TestCopyCTOR ]");
	}

	/**
	* Tests default constructor
	*/
	__forceinline char TestStringCTOR() {
		return Check(BigInteger("1000000000"), BigInteger(1000000000), "[ TestStringCTOR ]");
	}

#pragma endregion
#pragma region Arithmetic ops
	/**
	* Tests sum between 2 BigInteger
	*/
	__forceinline char TestSum() {
		char result = 1;

		result &= Check(BigInteger(100) + BigInteger(100), BigInteger(200), "[ TestSum [100 + 100] ]");
		result &= Check(BigInteger(100) + BigInteger(-100), BigInteger(0), "[ TestSum [100 + -100] ]");
		result &= Check(BigInteger(1000000) + BigInteger(1000000), BigInteger(2000000), "[ TestSum [1000000 + 1000000] ]");
		result &= Check(BigInteger(100) + BigInteger(-101), BigInteger(-1), "[ TestSum [100 + -101] ]");
		result &= Check(BigInteger(-100) + BigInteger(101), BigInteger(1), "[ TestSum [-100 + 101] ]");
		result &= Check(BigInteger(9) + BigInteger(-10), BigInteger(-1), "[ TestSum [9 + -10] ]");
		result &= Check(BigInteger(99) + BigInteger(-100), BigInteger(-1), "[ TestSum [99 + -100] ]");
		result &= Check(BigInteger(999) + BigInteger(-1000), BigInteger(-1), "[ TestSum [999 + -1000] ]");
		result &= Check(BigInteger(111) + BigInteger(-22), BigInteger(89), "[ TestSum [999 + -1000] ]");
		result &= Check(BigInteger(11111) + BigInteger(-333), BigInteger(10778), "[ TestSum [999 + -1000] ]");
		result &= Check(BigInteger(1234567890) + BigInteger(987654321), BigInteger(2222222211), "[ TestSum [1234567890 + 987654321] ]");
		return result;
	}

	/**
	* Tests sum between 2 BigInteger
	*/
	__forceinline char TestDiff() {
		char result = 1;
		result &= Check(BigInteger(-1000000) - BigInteger(999999), BigInteger(-1999999), "[ TestDiff ]");
		result &= Check(BigInteger(1000000) - BigInteger(-999999), BigInteger(1999999), "[ TestDiff Negate ]");
		result &= Check(BigInteger(9876543210) - BigInteger("9876543210"), BigInteger(0), "[ TestDiff 9876543210 - \"9876543210\"]");
		result &= Check(BigInteger(1) - BigInteger(250), BigInteger(-249), "[ TestDiff 1 - 250]");
		result &= Check(BigInteger(-1000) - BigInteger(-500), BigInteger(-500), "[ TestDiff -1000 - -500]");
		result &= Check(BigInteger(7777) - BigInteger(8888), BigInteger(-1111), "[ TestDiff 7777 - 8888]");
		result &= Check(BigInteger(-100000000000) - BigInteger(994827364324), BigInteger(-1094827364324), "[ TestDiff -100000000000 - 994827364324]");
		return result;
	}

	/**
	* Tests multiplication between 2 BigInteger
	*/
	__forceinline char TestMultiplication() {
		char result = 1;

		result &= Check(BigInteger(0) * BigInteger(10), BigInteger(0), "[ TestMultiplication [0*10] ]");
		result &= Check(BigInteger(2) * BigInteger(2), BigInteger(4), "[ TestMultiplication [2*2] ]");
		result &= Check(BigInteger(99) * BigInteger(99), BigInteger(9801), "[ TestMultiplication [99*99] ]");
		result &= Check(BigInteger(100) * BigInteger(99), BigInteger(9900), "[ TestMultiplication [100*99] ]");
		result &= Check(BigInteger(99) * BigInteger(100), BigInteger(9900), "[ TestMultiplication [99*100] ]");
		result &= Check(BigInteger(1234567890) * BigInteger(987654321), BigInteger(1219326311126352690), "[ TestMultiplication [1234567890*987654321] ]");
		result &= Check(BigInteger(-1) * BigInteger(2), BigInteger(-2), "[ TestMultiplication [-1*2] ]");
		result &= Check(BigInteger(-1) * BigInteger(-2), BigInteger(2), "[ TestMultiplication [-1*-2] ]");

		return result;
	}

	/**
	* Tests division between 2 BigInteger
	*/
	__forceinline char TestDivision() {
		BigInteger a = 10;
		BigInteger b = 10;
		char result = 1;

		result &= Check(BigInteger(408) / BigInteger(12), BigInteger(34), "[ TestDivision [408/12] ]");
		result &= Check(BigInteger(10) / BigInteger(5), BigInteger(2), "[ TestDivision [10/5] ]");
		result &= Check(BigInteger(5) / BigInteger(10), BigInteger(0), "[ TestDivision [5/10] ]");
		result &= Check(BigInteger(408) / BigInteger(-12), BigInteger(-34), "[ TestDivision [408/-12] ]");
		result &= Check(BigInteger(-408) / BigInteger(-12), BigInteger(34), "[ TestDivision [-408/-12] ]");
		result &= Check(BigInteger(-408) / BigInteger(12), BigInteger(-34), "[ TestDivision [-408/12] ]");
		return result;
	}

	/**
	* Tests pow of BigInteger
	*/
	__forceinline char TestPow() {
		BigInteger a = 10;
		char result = 1;

		result &= Check(pow(BigInteger(2), 2), BigInteger(4), "[ Test Pow [2^2] ]");
		result &= Check(pow(BigInteger(10), 100), BigInteger("10000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"), "[ Test Pow [10^100] ]");

		return result;
	}
#pragma endregion
}