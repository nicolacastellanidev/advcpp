#include "Specs.h"
#include "../../MemoryManagement/Nica/Print.h"

typedef char (*TestSpecMethod) ();

std::vector<TestSpecMethod> Tests = {
	BigIntSpecs::TestDefaultCTOR,
	BigIntSpecs::TestDefaultCTORWithNegative,
	BigIntSpecs::TestCopyCTOR,
	BigIntSpecs::TestStringCTOR,
	BigIntSpecs::TestSum,
	BigIntSpecs::TestDiff,
	BigIntSpecs::TestMultiplication,
	BigIntSpecs::TestPow,
	BigIntSpecs::TestDivision
};

int main()
{
	char result = 1;

	for (size_t i = 0; i < Tests.size(); ++i) {
		if (result == 0) {
			break;
		}
		result &= (Tests[i])();
	}

	Nica::PrintSpaceBetween({ "\n\nTESTS RESULT:" }, { result ? "[:)] [Test Passed]" : "[:(] [Test Failed]" }, 
		(result ? BACKGROUND_GREEN | BACKGROUND_BLUE : BACKGROUND_RED) | FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_BLUE);
}