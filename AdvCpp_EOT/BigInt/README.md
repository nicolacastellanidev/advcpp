# BigInt

This project shows a custom implementation for a big problem, ***infinite integers.***

## Solution adopted

I've decided to store *each digit* of an infinite int inside a ***deque***. 

This solution isn't the best one for space consumption as each digit will occupy an entire byte to store a number between 0-10 instead of 0-255.

But, the arithmetic operations are ***easier*** to write down as they are mainly class vertical operations.

I've tried different solutions and this is the best one so far for the complexity of **sums**, **multiplications** and other *math functions*.

## Getting started

Run ***main.cpp***, there are some tests to check arithmetic operations using the custom ***BigInteger*** class.

You should see no red tests if everything works fine.

## BigInteger.cpp

This is the **implemetation** class. Exposes some constructors and some useful overrides.